// IF -  Else
console.log("IF - Else");
var nama ="Rohman";
var peran ="Guard";

if(nama == '' && peran ==''){
    console.log("Nama dna Peran Harus Diisi!!");
}else if(nama !='' && peran ==''){
    console.log("Hallo " + nama + ", Pilih Peranmu untuk memulai game");
}else if(nama !='' && peran !=''){
    console.log("Selamat Datang di Dunia Werewolf, " + nama);
    if(peran=='penyihir'){
        console.log("Hallo "+ peran +" "+ nama + ",kamu dapat melihat siapa yang menjadi werewolf!");
    }else if(peran=='Guard'){
        console.log("Hallo "+ peran +" "+ nama + ",kamu akan membantu melindungi temanmu dari serangan werewolf.");
    }else if(peran=='werewolf'){
        console.log("Hallo "+ peran +" "+ nama + ",Kamu akan memakan mangsa setiap malam!");
    }
}

console.log("----------------------------------------------------------");
console.log("Switch - Case");
var hari="27";
var bulan= 1;
var tahun="1988";

switch(bulan){
    case 1: {
        console.log(hari + " Januari " + tahun);
        break;
    }
    case 2: {
        console.log(hari + " Pebruari " + tahun);
        break;
    }
    case 3: {
        console.log(hari + " Maret " + tahun);
        break;
    }
    case 4: {
        console.log(hari + " April " + tahun);
        break;
    }
    case 5: {
        console.log(hari + " Mei " + tahun);
        break;
    }
    case 6: {
        console.log(hari + " Juni " + tahun);
        break;
    }
    case 7: { 
        console.log(hari + " Juli " + tahun);
        break;
    }
    case 8: {
        console.log(hari + " Agustus " + tahun);
        break;
    }
    case 9: {
        console.log(hari + " September " + tahun);
        break;
    }
    case 10: {
        console.log(hari + " Oktober " + tahun);
        break;
    }
    case 11: {
        console.log(hari + " Nopember " + tahun);
        break;
    }
    case 12: {
        console.log(hari + " Desember " + tahun);
        break;
    }
}

