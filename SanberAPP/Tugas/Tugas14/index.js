import axios from 'axios';
import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Button, FlatList, TouchableOpacity, TextInput} from 'react-native';

const Styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'white'
  },
  header:{
    paddingTop:50,
    paddingHorizontal:16,
    backgroundColor:'grey',
    alignItems:'center'
  },
  title:{
    color:'white',
    fontSize:20
  },
  content1:{
    paddingHorizontal:16
  },
  input:{
    borderWidth:1,
    paddingVertical:10,
    paddingHorizontal:5,
    borderRadius:6,
    marginBottom:10
  },
  contentNew:{
    backgroundColor:'gray',
    paddingVertical:10
  }

})
export default function Crud(){
  const[title, setTitle]= useState("");
  const[value, setValue]= useState("");
  const[items, setItems]= useState([]);
  const[button, setButton]= useState("Simpan");
  const[selectedUser, setSelectedUser]= useState({});

  const submit = () =>{
    const data = {
      value, title
    }
    if(button=="Simpan"){
      //Jika Button Simpan
      axios.post(`https://achmadhilmy-sanbercode.my.id/api/v1/news`,data)
      .then(res=>{
        console.log('res :',res)
        setTitle("")
        setValue("")
        GetData()
      }).catch(err=>{
          console.log('error :', err)
      })
    }else{
      axios.put(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${selectedUser.id}`,data)
      .then(res=>{
        console.log('res :',res)
        setTitle("")
        setValue("")
        GetData()
        setButton("Simpan")
      }).catch(err=>{
        console.log('error :', err)
      })
    }
  }
  //ketika pilih berita
  const onSelectItem = (item) =>{
    console.log(item)
    setSelectedUser(item)
    setTitle(item.title)
    setValue(item.value)
    setButton("Update")
  }
  //ketika ambil data
  const GetData=()=>{
    axios.get(`https://achmadhilmy-sanbercode.my.id/api/v1/news`)
    .then(res=>{
      const data1=(res.data.data)
      console.log('res :',data1)
      setItems(data1)
    })
  }

  //ketika hapus data berdasarkan item tertentu
  const onDelete=(item)=>{
    axios.delete(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${item.id}`)
    .then(res=>{
      console.log('res :',res)
      GetData()
    }).catch(err=>{
      console.log('error :',err)
    })
  }

  //ambil data ketika jalan aplikasi
  useEffect(()=>{
    GetData()
  },[])

  return(
    <View style={Styles.container}>
      <View style={Styles.header}>
        <Text style={Styles.title}>API CRUD</Text>
      </View>
      <View style={Styles.content1}>
        <Text>Post Data</Text>
        <TextInput
          placeholder="Judul Berita"
          style={Styles.input}
          value={title}
          onChangeText={(value)=>setTitle(value)}
        />
        <TextInput
          placeholder="Isi Berita"
          style={Styles.input}
          value={value}
          onChangeText={(value)=>setValue(value)}
        />
        <Button
          title={button}
          onPress={submit}
        />
      </View>
      <View style={Styles.content1}>
        <Text>Data Berita</Text>
        
        <FlatList
          data={items}
          keyExtractor={(item, index)=>`${item.id}-${index}`}
          renderItem={({item})=>{
            return(
              <View>
                <TouchableOpacity onPress={()=>onDelete(item)}>
                  <Text style={{color:'red',alignSelf:'flex-end'}}>X</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>onSelectItem(item)}style={{borderRadius: 6,backgroundColor:'grey', padding: 5, marginBottom:10}}>
                  <Text style={{color:'white'}}>{item.title}</Text>
                  <Text style={{color:'white'}}>{item.value}</Text>
                </TouchableOpacity>
                </View>

            )
          }}
          />
      </View>
    </View>
  )
}
