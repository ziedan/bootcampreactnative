import React from 'react';
import { View, Image, StyleSheet,Text, TextInput, Button } from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
    alignItems:'center'
  },
  textLogin:{
    position: 'absolute',
    width: 375,
    height: 102,
    left: 0,
    top: 63,
    
  },
    textBiasa:{
    alignItems:'center',
    paddingTop:20,
  },
  profil:{
    flexDirection:'row',
    paddingTop:27,
    paddingHorizontal:24,
    paddingVertical:24,
    justifyContent:'flex-start'
  },
  box3: {
    justifyContent:'space-around',
    backgroundColor: '#AABBCC',
    width:300,
    alignContent:'flex-start',
    fontSize:10,
    
  },
  box4: {
    backgroundColor: '#AABBCC',
    width:300,
    alignContent:'flex-start',
    fontSize:10,
    
  }
});

const about = () => {
  return (
    <View style={styles.container}>
    <Text style={styles.textLogin}>Tentang Saya</Text>
      <Image
        source={require('./asset/person-circle-sharp.png')}
      />
        <Text style={styles.textLogin}>Mohamad Zennuri</Text>  
        <Text style={styles.textLogin}>Web Developer</Text>   
        <View style={[styles.box, styles.box3]}>
            <Text style={styles.profil}>Portofolio
            </Text>
        </View>
        <View style={[styles.box, styles.box4]}>
            <Text style={styles.profil}>Contact
            </Text>
        </View>
        
    </View>
  );
}

export default about;