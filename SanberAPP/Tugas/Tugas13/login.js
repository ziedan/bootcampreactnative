import React from 'react';
import { View, Image, StyleSheet,Text, TextInput, Button } from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
    alignItems:'center',
  },
  tinyLogo: {
    height: 50,
    width:200,
  },
  textLogin:{
      paddingTop:100,
      fontWeight: 'bold',
      color: 'black',
      fontSize:20,
  },
  input:{
      height:30,
      borderColor:'gray',
      borderWidth:1,
      width:200,
      borderRadius:9
  },
  textBiasa:{
      alignContent:'flex-start',
      paddingTop:20,
  },
  tombol:{
      width:40,
      height:40,
      padding:50
  }
 });

const Login = () => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.tinyLogo}
        source={require('./asset/lapakcode.png')}
      />
        <Text style={styles.textLogin}>LOGIN</Text>
        <Text style={styles.textBiasa}>Username :</Text>
        <TextInput style={styles.input}></TextInput>
        <Text style={styles.textBiasa}>Password :</Text>
        <TextInput style={styles.input}></TextInput>
        <Button
            style={styles.Button}
            title="Masuk"
            onPress={() => Alert.alert('Login pada Aplikasi')}
      />
    </View>
  );
}

export default Login;