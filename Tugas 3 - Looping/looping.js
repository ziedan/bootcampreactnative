console.log("Tugas Nomor 1 ---------------------------");
// Nomor 1 -----------------------------------
var counter=2;
var say="I Love Coding";
console.log("Looping Pertama");
while(counter <= 20){
    console.log(counter + " - " + say);
    counter+=2;
}

var counter2=20;
var say2="I will become a mobile developer";
console.log("Looping Kedua");
while(counter2>=2){
    console.log(counter2 + " - " + say2);
    counter2-=2;
}

// Nomor 2 ----------------------------------
console.log("");
console.log("Tugas Nomor 2 ------------------------");
for(var angka=1;angka<=20;angka++){
    if(angka%3==0 && angka%2>0){
        console.log(angka + " I Love Coding");
    }else if(angka%2>0){
        console.log(angka + " Santai");
    }else if(angka%2==0){
        console.log(angka + " Berkualitas");
    }
}

console.log("");
console.log("Tugas Nomor 3 ------------------------");
for(var x=1;x<=4;x++){
    for(var y=1; y<=4; y++){
       process.stdout.write("#");
    }
    console.log("");
}

console.log("");
console.log("Tugas Nomor 4 ------------------------");
for(var x2=1; x2<=7; x2++){
    for(var y2=1; y2<=x2; y2++){
        process.stdout.write("#");
    }
    console.log("");
}

console.log("");
console.log("Tugas Nomor 5 ------------------------");
for(var a=1;a<=8;a++){
    for(var b=1;b<=8;b++){
        if((a+b)%2==1){
            process.stdout.write(" ");
        }else{
            process.stdout.write("#");
        }
    }
    console.log("");
}