console.log("No 1 Range");
function range(startNum,finishNum){
    var ar =[];
    if(startNum<finishNum){
        for(var i=startNum; i<=finishNum;i++){
            ar.push(i);
        }
        ar.sort(function(value1,value2) {return value1-value2});
    }else if(startNum>finishNum){
        for(var i=startNum; i>=finishNum; i--){
            ar.push(i);
        }
        ar.sort(function(value1,value2){return value2-value1})
    }
    else{
        ar.push(-1);
    }
    return ar;
}
console.log(range(1,10));
console.log(range(10,1));
console.log(range(1));
console.log(range());

console.log("--------------------------------------------------------");
console.log("No 2 Range with step");
function rangeWithStep(startNum2, finishNum2, step)
{
    var ar2 =[];
    if(startNum2<finishNum2){
        for(var i2=startNum2; i2<=finishNum2;i2+=step){
            ar2.push(i2);
        }
        ar2.sort(function(value1,value2) {return value1-value2});
    }else if(startNum2>finishNum2){
        for(var i2=startNum2; i2>=finishNum2; i2-=step){
            ar2.push(i2);
        }
        ar2.sort(function(value1,value2){return value2-value1})
    }
    return ar2;
}
console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11,23,3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log("--------------------------------------------------------")
console.log("No 3 Sum Of Range");
function sum(startNum3, finishNum3, step3)
{
    if(step3==""){
        var z=1;       
    }else{
        var z=step3;
    }
    var ar3 =[];
    if(startNum3<finishNum3){
        for(var i3=startNum3; i3<=finishNum3;i3+=z){
            ar3.push(i3);
        }
        ar3.sort(function(value1,value2) {return value1-value2});
        var jumlah =  ar3.reduce(function(a,b){
            return a+b;
        },0);
    }else if(startNum3>finishNum3){
        for(var i3=startNum3; i3>=finishNum3; i3-=z){
            ar3.push(i3);
        }        
        ar3.sort(function(value1,value2){return value2-value1});
        var jumlah =  ar3.reduce(function(a,b){
            return a+b;
        },0);
    }else if(startNum3!=="" && finishNum3=="")
    {
        jumlah=1;
    }else{
        jumlah=0;
    }
    return jumlah;
}
console.log(sum(1,10,1))
console.log(sum(5, 50, 2)) 
console.log(sum(15,10,1)) 
console.log(sum(20, 10, 2)) 
console.log(sum(1)) 
console.log(sum())

console.log("--------------------------------------------------------")
console.log("No 4 Arraymultidimensi");
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(param){
    for(var satu=0;satu<=3;satu++){
            console.log("Nomor ID :" +param[satu][0]);
            console.log("Nama Lengkap :" +param[satu][1] );
            console.log("TTL :" +param[satu][2] + param[satu][3]);
            console.log("Hobi :" +param[satu][4]);
            console.log("");
        }
        
    }
dataHandling(input);

console.log("--------------------------------------------------------")
console.log("No 5 Balik Kata");
function balikKata(kata) {
    var iniKatanya = kata;
    var kataBaru = '';
   for (let i = kata.length - 1; i >= 0; i--) {
     kataBaru = kataBaru + iniKatanya[i];
    }
    
    return kataBaru;
   }
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log("--------------------------------------------------------")
console.log("No 6 Metode Array");
var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
function dataHandling2(datanya){
    datanya.splice(1,2,"Roman Alamsyah Elsharawy");
    datanya.splice(2,0,"Provinsi Bandar Lampung");
    datanya.splice(4,1,"Pria");
    datanya.splice(5,0,"SMA Internasional Metro")
    return datanya;    
}
console.log(dataHandling2(input2));
var tgl= input2[3].split("/");
var bln=(tgl[1]);
var bulan;
var namaBulan;
bulan=bln[1];
console.log(bulan);
switch(bulan){
    case 1: {
        namaBulan="Januari";
        break;
    }
    case 2: {
        namaBulan="Pebruari";
        break;
    }  
    case 3: {
        namaBulan="Maret";
        break;
    }
    case 4: {
        namaBulan="April";
        break;
    }    
    case 5: {
        namaBulan="Mei";
        break;
    } 
    case 6: {
        namaBulan="Juni";
        break;
    }   
}
console.log(namaBulan);
var tglUrut;
tglUrut = tgl.sort(function (value1, value2) { return value2 - value1 });
console.log(tglUrut);
var urutUlang = tgl.sort(function (value1, value2) { return value1 - value2 });
var slugtgl=urutUlang.join("-");
console.log(slugtgl)

var namaSaja=input2[1];
var namaPotong=namaSaja.slice(0,15);
console.log(namaPotong);







