import React, {useEffect, useState} from 'react';
import { FlatList, View } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title,Card, CardItem, Thumbnail, Text, Content, List,ListItem, Item} from 'native-base'
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'react-native-axios';

const spp=({navigation})=> {
    const [nisn, setNisn] =useState("");
    const [nama, setNama] =useState("");
    const [kelas, setKelas] = useState("");
    const [jurusan, setJurusan] = useState("");
    const [dataspp, setDataSpp] = useState([]);


    const renderspp =({item})=>{
      return(
        <View>    
            <ListItem >
              <Text >{item.bulan} - {item.tahun}</Text>
              {
                item.status=="LUNAS" &&
                <Button small rounded success  style={{position: 'absolute', right: 0}}>
                 <Text> {item.status} </Text>
              </Button>
              }
              {
                item.status=="BELUM" &&
                <Button small rounded danger  style={{position: 'absolute', right: 0}}>
                 <Text> {item.status} </Text>
              </Button>
              }
              
            </ListItem>
            </View>

      )
    }

    useEffect(()=>{
      AsyncStorage.getItem('nisn').then((value)=>{
         getSiswa(value);
         getSpp(value); 
        });
       
    },[])

    function getSpp(value){
      axios.get(`https://skip/sppsiswa.php?nisn=${value}`)
      .then(function(response){
         setDataSpp(response.data)
      })

  }

    function getSiswa(value){
        axios.get(`https://skip/getsiswa.php?nisn=${value}`)
        .then(function(response){
           setNisn(response.data.nisn)
            setNama(response.data.nama)
            setKelas(response.data.kelas)
            setJurusan(response.data.jurusan)
        })

    }
    return (
        <Container>
        <Header>
          <Body>
            <Title> SPP</Title>
          </Body>
          <Right>
            <Button transparent 
            onPress={()=>navigation.navigate('ProfileScreen')}
            >
              <Icon name='person-circle-outline' />
            </Button>
          </Right>
        </Header>
        <Content>
        <Card>
            <CardItem>
              <Left>
                <Thumbnail source={require('../asset/images/siswa.png')} />
                <Body>
                  <Text>{nama}</Text>
                  <Text note>{kelas} | {jurusan}</Text>
                </Body>
              </Left>
            </CardItem>
        </Card>
        <List>
           <FlatList 
           data={dataspp}
           renderItem = {renderspp}
           keyExtactor={(item,index)=>index.toSting()}
           />     
            
          </List>
        </Content>
      </Container>
    )
}

export default spp;