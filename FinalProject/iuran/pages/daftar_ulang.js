import React, {useEffect, useState} from 'react'
import { FlatList, View } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title,Card, CardItem, Thumbnail, Text, Content, List,ListItem, Item} from 'native-base'
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'react-native-axios';


const daftar_ulang=({navigation})=> {
    const [nisn, setNisn] =useState("");
    const [nama, setNama] =useState("");
    const [kelas, setKelas] = useState("");
    const [jurusan, setJurusan] = useState("");
    const [dataDu, setDataDu] = useState([]);

    useEffect(()=>{
      AsyncStorage.getItem('nisn').then((value)=>{
        getSiswa(value);
        getDu(value); 
       });

    },[])

    function getSiswa(value){
        axios.get(`https://skip/getsiswa.php?nisn=${value}`)
        .then(function(response){
          setNisn(response.data.nisn)
            setNama(response.data.nama)
            setKelas(response.data.kelas)
            setJurusan(response.data.jurusan)
        })
    }

    function getDu(value){
      axios.get(`https://skip/dusiswa.php?nisn=${value}`)
      .then(function(response){
        setDataDu(response.data)
      })
  }

  const renderDu=({item})=>{
    return(
      <View>    
            <ListItem >
              <Text > Daftar Ulang Untuk Tahun - {item.tahun}</Text>
              {
                item.status=="1" &&
                <Button small rounded success  style={{position: 'absolute', right: 0}}>
                 <Text> Lunas </Text>
              </Button>
              }
              {
                item.status=="" &&
                <Button small rounded danger  style={{position: 'absolute', right: 0}}>
                 <Text> Belum </Text>
              </Button>
              }
              
            </ListItem>
            </View>
    )
  }
    return (
        <Container>
        <Header>
          <Body>
            <Title> Daftar Ulang</Title>
          </Body>
          <Right>
            <Button transparent onPress={()=>navigation.navigate('ProfileScreen')}>
              <Icon name='person-circle-outline' />
            </Button>
          </Right>
        </Header>
        <Content>
        <Card>
            <CardItem>
              <Left>
                <Thumbnail source={require('../asset/images/siswa.png')} />
                <Body>
                  <Text>{nama}</Text>
                  <Text note>{kelas} | {jurusan}</Text>
                </Body>
              </Left>
            </CardItem>
        </Card>
        <List>
          <FlatList 
            data={dataDu}
            renderItem={renderDu}
            keyExtactor={(item,index)=>index.toSting()}
          />
        </List>
        
        </Content>
      </Container>
    )
}

export default daftar_ulang;