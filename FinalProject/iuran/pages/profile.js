import React, {useEffect, useState} from 'react';
import { View, Image, StyleSheet, BackHandler } from 'react-native';
import { Container, Header, Left, Body, Text, Button, Icon, Title, Content } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'react-native-axios';

export default function profile({navigation}) {

    const [nisn, setNisn] =useState("");
    const [nama, setNama] =useState("");
    const [kelas, setKelas] = useState("");
    const [jurusan, setJurusan] = useState("");
    const [tglLahir,setTglLahir]= useState("");

    useEffect(() => {
        AsyncStorage.getItem('nisn').then((value)=>{
            getSiswa(value);
        })
        },[])
    function getSiswa(value){
        axios.get(`https://skip/getsiswa.php?nisn=${value}`)
        .then(function(response){
           setNisn(response.data.nisn)
            setNama(response.data.nama)
            setKelas(response.data.kelas)
            setJurusan(response.data.jurusan)
            setTglLahir(response.data.tglLahir)
        })

    }

    function logout(){
        AsyncStorage.removeItem('nisn');
        BackHandler.exitApp();
    }
    return (
        <Container>
        <Header>
          <Left>
            <Button transparent onPress={()=> navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Profil Siswa</Title>
          </Body>
        </Header>
        <Content>
            <View style={styles.container}>
            <Image
            source={require('../asset/images/siswa.png')}
            style={{width: '30%', resizeMode: 'contain', margin: 0}}
            />
            <Text> {nama} </Text>
            <Text> {jurusan} </Text>
            <Text> {kelas} </Text>
            <Text> {tglLahir} </Text>
            <Button small rounded danger style={{alignSelf: 'center'}} onPress={()=>logout()} >
                <Text>Logout</Text>
            </Button>
            </View>
        </Content>
      </Container>
    )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#FFF',
    },
   
  });