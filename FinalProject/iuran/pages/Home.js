import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {Icon} from 'native-base';
import spp from './spp';
import daftar_ulang from './daftar_ulang';
import sumbangan from './sumbangan';
import Profile from './profile';
import { createStackNavigator } from '@react-navigation/stack';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

function SppStack() {
    return (
       <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="SppScreem" component={spp} />
         <Stack.Screen name="ProfileScreen" component={Profile} />
    </Stack.Navigator>
     );
  }

function duStack() {
    return (
       <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="duScreem" component={daftar_ulang} />
         <Stack.Screen name="ProfileScreen" component={Profile} />
    </Stack.Navigator>
     );
  }

  function sumbanganStack() {
    return (
       <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="sumScreem" component={sumbangan} />
         <Stack.Screen name="ProfileScreen" component={Profile} />
    </Stack.Navigator>
     );
  }

const Home=()=> {
  return (
    <NavigationContainer>
        <Tab.Navigator>
        <Tab.Screen 
            name="spp"
            component={SppStack}
            options={{
                tabBarLabel:"SPP",
                tabBarIcon:()=>(<Icon name="cash-outline" />)
            }}
            />
        <Tab.Screen 
            name="daftar_ulang"
            component={duStack}
            options={{
                tabBarLabel:"Daftar Ulang",
                tabBarIcon:()=>(<Icon name="save-outline" />)
            }}
            />
        <Tab.Screen
            name="sumbangan"
            component={sumbanganStack}
            options={{
                tabBarLabel:"Sumbangan",
                tabBarIcon:()=>(<Icon name="medkit-outline" />)
            }}
            />
        </Tab.Navigator>
    </NavigationContainer>
  );
}

export default Home