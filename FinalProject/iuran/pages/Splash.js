import React,{useEffect, useState}from 'react';
import {
    View,
    StyleSheet,
    Image, Text
  } from 'react-native';

const Splash =({navigation})=>{
    
    useEffect(()=>{
        setTimeout(() => {
            navigation.navigate('Login')
        }, 2000);
    },[])
    return(
        <View style={styles.container}>
      <Image
        source={require('../asset/images/logo.jpg')}
        style={{width: '40%', resizeMode: 'contain', margin: 0}}
      />
      <Text>INFORMASI PEMBAYARAN KEUANGAN SEKOLAH</Text>
      <Text>SMK AL KHOIRIYAH BARON</Text>
          </View>
    )
}

export default Splash;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#FFF',
    },
   
  });