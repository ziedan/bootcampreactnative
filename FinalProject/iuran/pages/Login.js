import React,{useState} from 'react';
import { Container,Icon,Text, Content, Form, Item, Input, Label, Button,Card, CardItem } from 'native-base';
import axios from 'react-native-axios';
import AsyncStorage from '@react-native-community/async-storage';

const Login =({navigation})=>{
    const [nisn,setNisn] = useState("");
    const [password,setPassword] = useState("");

    const prosesLogin=()=>{
        axios.get(`https://skip/login.php?nisn=${nisn}&password=${password}`)
        .then(function(response){
            if(response.data.status==1){
                //alert("Benar")
                AsyncStorage.setItem('nisn',response.data.nisn)
                navigation.navigate('Home')
            }else{
                alert('Password atau username Salah')
            }
        }) 
               
    }
    return(
       <Container>      
           <Card transparent>
            <CardItem header >
              <Text >Login Aplikasi</Text>
            </CardItem>  
            </Card>
           <Content padder>              
           <Form>
            <Item stackedLabel>
              <Label>Username</Label>
              <Input onChangeText={(user)=>setNisn(user)}/>
            </Item>
            <Item stackedLabel last>
              <Label>Password</Label>
              <Input secureTextEntry={true} onChangeText={(psw)=>setPassword(psw)}/>
            </Item>
          </Form>
                <Button block style={{marginTop:10}} success iconLeft onPress={prosesLogin}><Icon name="log-in-outline"/><Text upercase={false}>Login</Text></Button>
           </Content>
       </Container>
    )
}

export default Login;

