import 'react-native-gesture-handler';
import React from 'react';
import { createAppContainer, createSwitchNavigator} from 'react-navigation';
import Splash from './pages/Splash';
import Login from './pages/Login';
import Home from './pages/Home'

const App = createSwitchNavigator({
  Splash:{    
    screen:Splash,
    navigationOptions:{
      headerShown :false,
    }
  },
  Login:{
    screen:Login,
    navigationOptions:{
      headerShown :false,
    }
  },
  Home:{
    screen:Home,
    navigationOptions:{
      headerShown :false,
    }
  } ,
  
  
 
})

export default createAppContainer(App);