function readBookPromise(time,book){
    console.log(`Saya mulai membaca ${book.name}`)
    return new Promise(function(resolve,reject){
        setTimeout(function(){
            let sisaWaktu = time -book.temeSpent
            if(sisaWaktu >=0){
                console.log(`saya sudah selesai membaca buku ${book.name}, sisa waktu saya ${sisaWaktu}`)
                resolve(sisaWaktu)
            }else{
                console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`)
                reject(sisaWaktu)
            }
        },book.timeSpent)
    })
}