console.log("Nomor 1 --------------------------------------")
const golden =()=> console.log("This Is Golden!!")
golden();

console.log("")
console.log("Nomor 2 --------------------------------------")
const newFunction = (firstName,lastName)=>({
    firstName:firstName,
    lastName:lastName,
    fullName:function(){
        console.log(firstName + " "+ lastName)
    }
})
newFunction("William", "Imoh").fullName() 

console.log("")
console.log("Nomor 3 --------------------------------------")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const { firstName, lastName,destination,occupation,spell } = newObject;
console.log(firstName, lastName, destination, occupation)

console.log("")
console.log("Nomor 4 --------------------------------------")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [west,east]
console.log(combined)


console.log("")
console.log("Nomor 5 --------------------------------------")
let planet="earth"
let view ="glass"
let before=`Lorem  ${view} dolor sit amet,consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before)