console.log("Release 1 --------------")
class Animal {
    constructor(name){
        this.name=name
        this.legs=4
        this.cold_blooded="false"
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


console.log("Release 2 --------------")
class Frog extends Animal{
    constructor(name){
        super(name)
        
    }
    Jump() {
        return console.log("Hop Hop")
    }
}

class Ape extends Animal{
    constructor(name){
        super(name)
    }
    Yell() {
        return console.log("Auoooo")
    }
}
var sungokong = new Ape("kera sakti")
sungokong.Yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.Jump() // "hop hop"


console.log("Nomor 2-------------------")
class Clock{
    constructor({template}, timer){
        this.timer = timer;
                this.template = template;
     
    }
    render(){
        let date = new Date();

        let hours = date.getHours();
        if(hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if(mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if(secs < 10) secs = '0' + secs;


        let output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output)
    }

    stop(){
        clearInterval(this.timer);
    }

    start(){
        this.render()
      this.timer = setInterval(this.render.bind(this), 1000);
    }
}

let clock = new Clock({template: 'h:m:s'});
clock.start();