console.log("Soal No. 1 ---------------");
function teriak(){
    return "Hallo Sanbers!";
}
console.log(teriak());
console.log("");

console.log("Soal No 2 -----------------");
function kalikan(param1,param2){
    return param1 * param2;
}

var num1= 12;
var num2= 4;
var hasilkali = kalikan(num1,num2);
console.log(hasilkali);

console.log("");

console.log("Soal No 3 -----------------");
function introduce(nama,usia,alamat,hobi){
    return "Nama Saya "+ nama + ", Umur Saya "+ usia +" Tahun, Alamat Saya "+ alamat + ", dan saya punya hobby yaitu "+ hobi;
}

var name = "Agus";
var age = 30;
var address ="Jl. Malioboro, Yogyakarta";
var hobby ="gaming";

var perkenalan = introduce(name,age,address,hobby);
console.log(perkenalan);